from duo_auth.backends import DuoOIDCBackend

from uw_saml_tools.utils import sync_user_groups


class DuoBackend(DuoOIDCBackend):
    def handle_groups(self, user, groups):
        sync_user_groups(user, groups)
