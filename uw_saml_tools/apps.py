from django.apps import AppConfig


class UwSamlToolsConfig(AppConfig):
    name = 'uw_saml_tools'
