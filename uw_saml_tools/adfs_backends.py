from django.conf import settings
from django.contrib.auth import get_user_model

from django_auth_adfs.config import settings as adfs_config
from django_auth_adfs.backend import AdfsAuthCodeBackend
from django_auth_adfs import signals


from uw_saml_tools.utils import sync_user_groups

import logging

logger = logging.getLogger("uw_saml_tools")

class ADFSBackend(AdfsAuthCodeBackend):

    def process_access_token(self, access_token, adfs_response=None):
        if not access_token:
            raise PermissionDenied

        logger.debug("Received access token: %s", access_token)
        claims = self.validate_access_token(access_token)
        if not claims:
            raise PermissionDenied

        if getattr(settings, 'AUTO_CREATE_USER', True):
            user = self.create_user(claims)
        else:
            usermodel = get_user_model()


            try:
                user = usermodel.objects.get(**{usermodel.USERNAME_FIELD: claims[adfs_config.USERNAME_CLAIM]})
                if not user.password:
                    user.set_unusable_password()
            except usermodel.DoesNotExist:
                return None

        groups = self.process_user_groups(claims, access_token)
        self.update_user_attributes(user, claims)
        self.update_user_groups(user, groups)
        self.update_user_flags(user, claims, groups)

        signals.post_authenticate.send(
            sender=self,
            user=user,
            claims=claims,
            adfs_response=adfs_response
        )



        user.full_clean()
        user.save()
        return user

    def update_user_groups(self, user, claim_groups):
        if adfs_config.GROUPS_CLAIM is not None:

            sync_user_groups(user, claim_groups)
