from djangosaml2.backends import Saml2Backend as djsaml2Backend
from django.conf import settings

from uw_saml_tools.utils import sync_user_groups

class Saml2Backend(djsaml2Backend):

    def update_user(self, user, attributes, attribute_mapping, force_save=False):
        if not attribute_mapping:
            return user

        to_remove = []

        for saml_attr, django_attrs in attribute_mapping.items():
            if 'groups' in django_attrs:
                group_list = attributes.get(saml_attr)
                if group_list is not None:
                    sync_user_groups(user, group_list)
                    to_remove.append(saml_attr)

        for remove in to_remove:
            del attributes[remove]

        return super(Saml2Backend, self).update_user(user, attributes, attribute_mapping, force_save)

