from django.contrib.auth.models import Group
from django.db import models


class SyncGroup(models.Model):
    identifier = models.CharField(max_length=1024, null=False, blank=False, help_text="Group identifier provided by saml")
    group = models.ForeignKey(Group, related_name="group_sync", on_delete=models.CASCADE)

    def __str__(self):
        return self.identifier
