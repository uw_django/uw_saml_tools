from django.contrib.auth.models import Group
from uw_saml_tools.models import SyncGroup


def sync_user_groups(user, saml_list):
    """
    Takes a django user and list of groups as provided by saml and syncs the
    user's group membership to match based on GroupSync configurations.

    Any group that does not have a GroupSync configuration will not be
    affected.
    """
    groups = []

    for g in Group.objects.filter(user=user):
        if g.group_sync.count() > 0:
            groups.append(g)

    sync_groups = SyncGroup.objects.filter(identifier__in=saml_list)

    distinct_sync = set([sg.group for sg in sync_groups])

    remove_groups = set(groups).difference(distinct_sync)
    add_groups = distinct_sync.difference(set(groups))

    for rg in remove_groups:
        user.groups.remove(rg)

    for ag in add_groups:
        user.groups.add(ag)
