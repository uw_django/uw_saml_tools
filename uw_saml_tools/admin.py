from django.contrib import admin

from uw_saml_tools.models import SyncGroup

@admin.register(SyncGroup)
class SyncGroupAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'group',)
