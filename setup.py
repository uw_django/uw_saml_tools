try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
setup(name='uw_saml_tools',
      version='1.0',
      description='UW Saml Tools',
      author='Ryan Goggin',
      author_email='ryan.goggin@uwaterloo.ca',
      url='https://ryangoggin.net',
      packages=['uw_saml_tools'],
      package_dir={'uw_saml_tools': 'uw_saml_tools'},
     )
