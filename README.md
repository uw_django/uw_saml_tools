# UW Django SAML Tools

Provides saml tools for Django, include `uw_saml_tools` in `INSTALLED_APPS`.

Requires django-auth-adfs (https://django-auth-adfs.readthedocs.io/en/latest/)

# Tools

- Group Membership Sync
  - On login, a users groups will be synced based on groups reported by SAML


# Auto Create User

By default, users who successfully authenticate will be created. You can disable this by setting

`AUTO_CREATE_USER = False`

in your django settings

# Login Failed

If you disable auto creation, users who don't have an account will receive an login error page.  If you wish to override this page, you need a template in the following location `<app>/templates/django_auth_adfs/login_failed.html`.  Ensure that `<app>` if located ABOVE `django_auth_adfs` in `INSTALLED_APPS`
